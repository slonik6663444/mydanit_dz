"use strict"

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set name(name) {
        this._name = name;
    }

    get name() {
        return this._name;
    }

    set age(age) {
        this._age = age;
    }

    get name() {
        return this._age;
    }
    set salary(salary) {
        this._salary = salary;
    }

    get salary() {
        return this._salary;
    }
}
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    set salary(salary) {
        this._salary = (salary * 3).toString();
    }

    get salary() {
        return this._salary;
    }
}

let employee = new Employee("Alisa", "30", "7000");

let junior = new Programmer("Kirill", "24", "3000", "sql");
let middle = new Programmer("Maxim", "26", "4000", "c++");
let senior = new Programmer("Kostya", "28", "5000", "java");

console.log(junior);
console.log(middle);
console.log(senior);





/*Теоретический вопрос*/
// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
//
// С помощью прототипного наследования в JS можно создавать новые объекты на основе уже существующих.
// При этом новые объекты могут использовать те же методы и свойства, которые есть в родительских
// объектах. Также дочерние объекты могут иметь свои свойства и методы, не имеющие отношения к
// родительским объектам.
    



