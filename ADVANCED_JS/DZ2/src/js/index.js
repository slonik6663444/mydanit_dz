"use strict"

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function arrToList(books) {
    let ul = document.createElement("ul");
    let root = document.getElementById("root");
    root.prepend(ul);
    books.forEach((obj) => {
        try {
            ['author', 'name', 'price'].forEach((key) => {
                if (!(key in obj)) {
                    throw new Error(`Error: is not ${key}!!!`);
                }
            })
            const {author, name, price} = obj;
            let li = document.createElement("li");
            let newArrList = document.getElementsByTagName("ul");
            newArrList[0].append(li);
            li.innerText = `${author} ${name} ${price}`;
        } catch (e) {
            console.log(e.message);
        }
    });
}

arrToList(books);


/*Теоретический вопрос*/
// Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.

// Данную конструкцию уместно использовать при обработке непредусмотренных ошибок,
// при этом обеспечивая более стабильную реализацию приложения.
// При обработке данных с помощью метода "JSON.parse()" очень часто используется
// try...catch, так как скрипт при этом не "упадет", даже если данные не являются
// корректными.



