
"use strict"

fetch("https://swapi.dev/api/films/")
    .then((response) => response.json())
    .then((data) => {
        charactersPusher(data);
    })
    .catch((error) => console.log(error.message));

const louder=(id)=> {
    return`<div class="container" id="${id}">
    <div class="item-1"><div></div></div>
    <div class="item-2"><div></div></div>
    <div class="item-3"><div></div></div>
    <div class="item-4"><div></div></div>
    <div class="item-5"><div></div></div>
    <div class="item-6"><div></div></div>
    <div class="item-7"><div></div></div>
    <div class="item-8"><div></div></div>
    <div class="item-9"><div></div></div>
</div>`
}

function infoList() {
    const ul = document.createElement("ul");
    const li = document.createElement("li");
    const episode = document.createElement("p");
    const title = document.createElement("h3");
    const character = document.createElement("p");
    const openingCrawl = document.createElement("p");
    li.append(episode, title, character, openingCrawl);
    return {ul, li}
};
function createCharactersList(characterNames) {
    const listNames = document.createElement("ul");
    characterNames.forEach((character, index) => {
        const nameItem = document.createElement("li");
        nameItem.textContent = character;
        listNames.append(nameItem);
    });
    return listNames
}

function filmPusher({title, episode_id, opening_crawl}) {
    let {ul, li} = infoList();
    let listItem = li.cloneNode(true);
    listItem.children[0].textContent = episode_id;
    listItem.children[1].textContent = title;
    listItem.children[2].innerHTML =louder(episode_id);
    listItem.children[3].textContent = opening_crawl;
    ul.append(listItem);
    document.body.append(ul);
};

function charactersPusher({results}) {
    const objPromises = {};
    results.forEach((film) => {
        if (!objPromises[film.title]) objPromises[film.title] = {film, characters: []};
        filmPusher(film);
        film.characters.forEach((url) => {
            const request = fetch(url)
                .then((response) => response.json())
                .catch((error) => console.log(error.message));
            objPromises[film.title].characters.push(request);
        })
    })

    const resultArr = Object.entries(objPromises);
    resultArr.forEach(([title, {characters, film}]) => {
         Promise.all(characters)
            .then(function (response) {
                const characters = response.map(({name}) => name)
                const episodId = document.getElementById(film.episode_id);
                const li = document.createElement("li");
                li.append(createCharactersList(characters));
                episodId.parentNode.replaceChild( li,episodId);
                
            })
    })


}






//теоретический вопрос
//Объясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.
//AJAX - это асинхронный Javascript, который используется для обработки запросов с
//сервера, а также запуска приложений асинхронно.
