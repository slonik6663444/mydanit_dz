import {VisitCardiologist, VisitDentist, VisitTherapist} from './visit.js';
// import {DragDrop} from "./DragDrop.js";

export class Card {
    constructor(visitCard) {

        this.id = visitCard.id;
        console.log(this.id);
        this.visitCard = visitCard;
        console.log(visitCard);

        this.elements = {
            cardContainer: document.createElement("div"),
            editBtn: document.createElement('button'),
            detailsBtn: document.createElement('button'),
            deleteBtn: document.createElement('button'),
            doctorDetailsFields: document.createElement('div'),
            editCardContainer: document.createElement("div")
        }
    }
    render() {
        this.elements.cardContainer.classList.add('card');
        this.elements.editBtn.classList.add('edit-btn');
        this.elements.detailsBtn.classList.add('details-btn');
        this.elements.deleteBtn.classList.add('delete-btn');
        this.elements.doctorDetailsFields.classList.add('doctorDetails');
        this.elements.editCardContainer.classList.add('editCardContainer');
        this.elements.editBtn.innerText = 'Edit';
        this.elements.detailsBtn.innerText = 'Details';
        this.elements.deleteBtn.innerText = 'X';


        /*   loop to fill cards fields */

        const cardContentInfo = Object.entries(this.visitCard);
        console.log(cardContentInfo);
        cardContentInfo.forEach(([key, value]) => {

            if (key.toLowerCase() === "age" || key.toLowerCase() === "bodyindex" ||
                key.toLowerCase() === "illnesses" || key.toLowerCase() === "pressure" ||
                key.toLowerCase() === "lastvisit" || key.toLowerCase() === "id") {

                let detailsCardField = document.createElement("p");
                detailsCardField.classList.add("card-fields");
                detailsCardField.innerText = `${key.toUpperCase()}: ${value}`;
                this.elements.doctorDetailsFields.append(detailsCardField);

            } else {
                let mainCardField = document.createElement("p");
                mainCardField.classList.add("card-fields");
                cardContentInfo.forEach((items) => {
                    let cardList = document.createElement('ul');
                    console.log(items);
                    let item = [...items];
                    console.log(item[1]);
                    let cardData = (item[1]);
                    console.log(cardData);
                    Object.entries(cardData).forEach(([key, value]) => {
                        let cardListItem = document.createElement('li');
                        console.log(`${key} ${value}`);
                        cardListItem.innerText = `${value}`;
                        cardList.append(cardListItem);
                        mainCardField.append(cardList);
                        console.log(cardList);
                    })
                    this.elements.cardContainer.append(mainCardField);
                })
            }
        })

        this.elements.cardContainer.append(this.elements.editBtn, this.elements.deleteBtn);
        this.elements.cardContainer.insertAdjacentElement('beforeend', this.elements.detailsBtn);
        document.querySelector('.task-container').append(this.elements.cardContainer);


        /*   Card Btn Listeners   */

        this.elements.deleteBtn.addEventListener('click', () => this.deleteCardRequest());
        this.elements.detailsBtn.addEventListener('click', () => {
            if (!document.querySelector('.doctorDetails')) {
                this.elements.cardContainer.append(this.elements.doctorDetailsFields);
                this.elements.detailsBtn.innerText = 'Hide';
            } else {
                this.elements.detailsBtn.innerText = 'Details';
                document.querySelector('.doctorDetails').remove();
            }
        })
        this.elements.editBtn.addEventListener('click', () => this.editCard());
        // DragDrop('.card');
    }

    deleteCardRequest() {
        fetch(`https://ajax.test-danit.com/api/cards/${this.id}`, {
            method: 'DELETE',
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                // 'Authorization': `Bearer ${localStorage.getItem('tokenData')}`
                'Authorization': `Bearer 26155636-372f-421a-ab2a-89039bba2bbf`
            },
        }).then((res) => {
            if (res.status >= 200 && res.status < 300) {
                return res;
            } else {
                let error = new Error(res.statusText);
                error.response = res;
                throw error
            }
        })
            .then((response) => response.json())
            .then(response => {
                if (response.status === 'Success') {
                    this.elements.cardContainer.remove();
                    this.elements.deleteBtn.removeEventListener('click', () => this.deleteCardRequest());
                }
            })
    }

    editCard() {
        const removeBtn = document.querySelectorAll(".edit-btn");
        removeBtn.forEach(e => e.classList.add("remove"));

        if (this.visitCard.title.toLowerCase() === "Кардиолог") {
            document.querySelector('.task-container').append(this.elements.editCardContainer);
            const editFormCardiologist = new VisitCardiologist(this.visitCard);
            editFormCardiologist.render(this.elements.editCardContainer);
        }
        if (this.visitCard.title.toLowerCase() === "Терапевт") {
            document.querySelector('.task-container').append(this.elements.editCardContainer);
            const editFormVisitTherapist = new VisitTherapist(this.visitCard);
            editFormVisitTherapist.render(this.elements.editCardContainer);
        } else if (this.visitCard.title.toLowerCase() === "Дантист") {
            document.querySelector('.task-container').append(this.elements.editCardContainer);
            const editFormVisitDentist = new VisitDentist(this.visitCard);
            editFormVisitDentist.render(this.elements.editCardContainer);
        }
    }
}

