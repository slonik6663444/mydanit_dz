// import { Input } from './input.js';
// import { Button } from './button.js';
// import { Select } from './select.js';
//
// export class CreateVisitModal {
//   constructor(selecort) {
//     this.$el = selecort;
//     this.modalElement = document.getElementById('openModal');
//     this.open=this.open.bind(this)
//     this.close=this.close.bind(this)
//   }
//   createVisitModal () {
//     this.modalElement.className = "modal";
//     const modalWrap = document.getElementById(this.$el);
//     const modal = document.createElement('div');
//     modalWrap.appendChild(modal);
//
//     const buttonSelectDoctor = new Button(
//         'modal',
//         'entryButton',
//         'entryButton',
//         'Select doctor'
//     );
//     buttonSelectDoctor.createBtn();
//     // this.open=this.open.bind(this)
//     // this.close=this.close.bind(this)
//   }
//
//   open() {
//     this.modalElement.style.position='fixed'
//     this.modalElement.style.opacity = 1;
//   }
//   close() {
//     this.modalElement.style.position='static'
//     this.modalElement.style.opacity = 0;
//   }
// }
//
// class LoginModal {
//   constructor(selecort) {
//     this.$el = selecort;
//   }
//
//   createModal() {
//     const modalWrap = document.getElementById(this.$el);
//     const modal = document.createElement('div');
//     modal.className = 'modal';
//     modal.id = 'modal';
//     modalWrap.appendChild(modal);
//     const form = document.createElement("form");
//     form.id = "register-form";
//     form.setAttribute("action", "#");
//     const login = new Input(
//         'modal',
//         'login__input',
//         'login__input',
//         'Enter Login'
//     );
//     login.createInput();
//     const password = new Input(
//         'modal',
//         'password__input',
//         'password__input',
//         'Enter Password'
//     );
//     password.createInput();
//
//     const entryButton = new Button(
//         'modal',
//         'entryButton',
//         'entryButton',
//         'Entry'
//     );
//     entryButton.createBtn()
//     form.append(this.login, this.password, this.entryButton);
//   }
// }
//
// const newModal = new LoginModal('root');
// newModal.createModal();
//
// const openLoginModal=document.getElementById('entry')
// openLoginModal.addEventListener("click", () => {
//   const newModal = new LoginModal('root');
//   newModal.createModal();
// })


const loginUrl = 'https://ajax.test-danit.com/api/cards/login';
const token = sessionStorage.getItem('token');
console.log(token);

export class Modal {
  constructor(email, password, submitBtn) {
    //Create DOM elements
    this.form = document.createElement('form');
    this.closeBtn = document.createElement('button');
    this.formBody = document.createElement('div');
    this.email = document.createElement('input');
    this.password = document.createElement('input');
    this.submitBtn = document.createElement('button');
  }

  closeSigningForm(){
    document.body.addEventListener('click',(event)=>{
      if(event.target.classList.value===('signInForm')){
        this.form.style.display='none';
      }
    })
  }

  render() {
    //
    this.form.className = "signInForm"
    this.closeBtn.className = "signInForm__closeBtn";
    this.formBody.className = "signInForm__body";
    this.email.className = "signInForm__email";
    this.password.className = "signInForm__password";
    this.submitBtn.className = "signInForm__submitBtn";

    this.closeBtn.textContent = "X";
    this.submitBtn.textContent = "Confirm";

    this.email.setAttribute("type", "email");
    this.password.setAttribute("type", "password");
    this.submitBtn.setAttribute("type", "submit");

    this.email.name = 'email';
    this.password.name = 'password';

    this.email.placeholder = "Enter your - email";
    this.password.placeholder = "Enter your - password";

    this.formBody.append(this.closeBtn, this.email, this.password, this.submitBtn);
    this.form.append(this.formBody);
    document.body.prepend(this.form);

    this.form.addEventListener('keypress', (event) => {
      const key = event.code;
      if (key === "Enter") {
        this.getLogin(this.email, this.password, this.form);
      }
    })

    this.submitBtn.addEventListener('click', (event) => {
      event.preventDefault();
      this.getLogin(this.email, this.password, this.form);
      document.querySelector('.signInForm').remove();
    })

    this.closeBtn.addEventListener('click', (event) => {
      event.preventDefault();
      this.form.style.display = "none";
    })
    this.closeSigningForm();
  }

  changeBtn() {
    document.querySelector('.header__login-button').style.display = "none";
    document.querySelector('.header__create-button').style.display = "block";
  }

  async getLogin(email, password, form) {
    await fetch(loginUrl, {
      method: 'POST',
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
      body: JSON.stringify({
        "email": email.value,
        "password": password.value
      }),
    })
        .then(response => {
          return (response.text())
        })
        .then(token => {
          if (typeof token!=='string') {
            throw new Error('Введен неверный логин или пароль')
          } else {
            localStorage.setItem('token', token)
            // sessionStorage.setItem('token', response.token);
            // form.style.display = 'none';
            // this.changeBtn();
            // const bord = new TaskDesk().init();
          }
        })
        .catch(error => {
          console.log('uuuuuuu')
          alert(error);
        })
  }
}

export {token};

const loginButton = document.getElementById("entry")
loginButton.addEventListener('click', async (event) => {
  const modal = new Modal().render();
})
//
// this.createButton.addEventListener('click', async (event) => {
//   if (!document.querySelector('.doctor-select')) {
//     const form = new Form().render();
//   }
//   document.querySelector('.doctor-select').className = 'doctor-select';
// })
