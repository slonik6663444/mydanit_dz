"use strict"

const buttonSearhIp = document.getElementById("buttonSearchIp");
buttonSearhIp.addEventListener("click", getIp);

async function getIp() {
    try {
        const data = await fetch("https://api.ipify.org/?format=json");
        const {ip} = await data.json();
        const sendRequest = await fetch(`http://ip-api.com/json/${ip}`);
        const fisicalAddressData = await sendRequest.json();
        renderFisicalAddressData(fisicalAddressData);
    } catch (error) {
        console.log(error.message);
    }
}

function renderFisicalAddressData({timezone, country, regionName, city, zip}) {
    const adresslist = document.createElement("ul");
    const renderAddress = {timezone, country, regionName, city, zip};
    Object.entries(renderAddress).forEach(([key, value]) => {
        const adressItem = document.createElement("li");
        adressItem.textContent = (`${key}: ${value}`);
        adresslist.append(adressItem);
    });
    const root = document.body.querySelector("#root");
    root.textContent = "";
    root.append(adresslist);
    }



/*
Теоретический вопрос:
Обьясните своими словами, как вы понимаете асинхронность в Javascript
Асинхронность в Javascript позволяет нам выполнять задачи не по порядку,
а в таком порядке как это нужно для корректной работы приложения.
 */