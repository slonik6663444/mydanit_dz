"use strict"

let tab = function() {
    let tabsTitle = document.querySelectorAll(".tabs-title");
    let tabsContent = document.querySelectorAll(".tabs__pane");
    let tabName;

    tabsTitle.forEach(item => {
        item.addEventListener("click", selectTabTitle)
    })
    function selectTabTitle() {
        tabsTitle.forEach(item => {
            item.classList.remove("active");
        });
       this.classList.add("active");
       tabName = this.getAttribute("data-tab-name");
        selectTabsContent(tabName);
        console.log(tabName);
    }
    function selectTabsContent(tabName) {
        tabsContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add("tabs__pane_active") : item.classList.remove("tabs__pane_active");
        })
    }
};

tab();
























