"use strict"

jQuery("document").ready(function($){
    let nav = $('.nav-container');
    $(window).scroll(function () {
        if ($(this).scrollTop() > 136) {
            nav.addClass("f-nav");
        } else {
            nav.removeClass("f-nav");
        }
    });
});

let last_id;
let $top_menu = $('.nav-container');
let menu_height = $top_menu.outerHeight(true);
let $menu_items = $top_menu.find('a');
let $scroll_items = $menu_items.map(function(){
    var item = $($(this).attr('href'));
    if (item.length){
        return item;
    }
});

$menu_items.click(function(e){
    var href = $(this).attr('href'),
        offset_top = href === '#' ? 0 : $(href).offset().top - menu_height;
    $('html, body').stop().animate({
        scrollTop: offset_top
    }, 300);
    e.preventDefault();
});

$(window).scroll(function(){
    var from_top = $(this).scrollTop() + menu_height;
    var mar = parseInt($top_menu.css('margin-bottom'));
    var cur = $scroll_items.map(function(){
        if ($(this).offset().top < from_top + mar){
            return this;
        }
    });
    cur = cur[cur.length - 1];
    var id = cur && cur.length ? cur[0].id : '';
    if (last_id !== id){
        last_id = id;
        $menu_items.parent()
            .removeClass('active')
            .end()
            .filter("[href='#" + id + "']")
            .parent()
            .addClass('active');
    }
});
$(document).ready(function(){

    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

});
$( document ).ready(function(){
    $(".slide-toggle").click(function(){
        $(".slide_toggle").slideToggle( 1000, "linear", function(){
            console.log("slideToggle completed");
        });
    });
});















