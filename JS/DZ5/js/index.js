"use strict"

function createNewUser() {
    const newUser = {
        firstName: prompt("Enter your name"),
        lastName: prompt("Enter your last name"),
        birthday: prompt(" Enter your birthday / dd.mm.yyyy"),

        getLogin() {
            return ((this.firstName[0] + this.lastName).toLowerCase());
        },

        getAge() {
            const now = Date.now();

            const dd = this.birthday.substring(0, 2);
            const mm = this.birthday.substring(3, 5);
            const yyyy = this.birthday.substring(6, 10);

            const birthDate = new Date(yyyy, mm - 1, dd);

            const birthdayMs = Date.parse(birthDate);

            const userAge = parseInt((now - birthdayMs) / 3.154e+10);
            return (userAge);
        },
        getPassword() {
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6, 10));
        }
    };
    return newUser;
}

const result = createNewUser();


console.log(result.getLogin(), result.getAge(), result.getPassword());


/*
Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования

Экранирование - способ размещения внутри литерала символов окончания литерала, а также непечатаемых и специальных
символов. Метка для вопросов, связанных с проблемами экранирования символов в различных языках программирования, разметки и т. д.

Работает экранирование при вставке / перед экранируемым символом. Таким образом, можно добавлять специальные символы, а также
прописывать их восьмеричный либо шестнадцатиричный код.
 */