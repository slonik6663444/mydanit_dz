"use strict"

let arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
function arrToList(arr) {
    let ul = document.createElement("ul");
    let root = document.getElementById("root");
    root.prepend(ul);

    let newArr = arr.map((elements) => {
        let li = document.createElement("li");
        let newArrList = document.getElementsByTagName("ul");
        newArrList[0].append(li);
        li.innerText = `${elements}`;
        return newArrList;
    });
}
arrToList(arr);

/*
Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)

DOM - объектная модель документа. В этой модели отображаются все параметры и свойства, которые при необходимости
можно изменить. Имеет древообразную структуру.

 */