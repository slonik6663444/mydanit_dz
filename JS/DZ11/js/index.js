"use strict"
const btn = document.getElementById('button');
document.addEventListener('keypress', (event) => {
    const keyName = event.key;

    btn.childNodes.forEach((item) => {
        if (item.classList && item.classList.contains("btn")){
            const active = item.textContent.toUpperCase() === keyName.toUpperCase();
            item.style = `background-color: #${active ? "3030d4" : "33333a"} ;`;
        }
 })
});

//
// Почему для работы с input не рекомендуется использовать события клавиатуры?
//
//     Так как пользователь может вводить данные не используя клавиатуру. Например, вставить текст из буфера обмена.
//     А также при работе с мобильными устройствами, события клавиатуры могут не распознаваться.
//




















