"use strict"

const input = document.createElement("input");
input.setAttribute("type", "number");
input.setAttribute("placeholder", "Price");
input.setAttribute("id", "price");
let root = document.getElementById("root");
root.append(input);

input.addEventListener("focus", (event) => {
    input.style.border = "5px solid green";
});

let span, button, paragraph;

input.addEventListener("blur", (event) => {
    if (input.value < 0) {
        input.style.border = "5px solid red";
        paragraph = document.createElement("p");
        paragraph.setAttribute("id", "priceErr")
        paragraph.innerText = "Please enter correct price";
        root.after(paragraph);

    } else if (input.value > 0) {
        span = document.createElement("span");
        span.setAttribute("id", "priceSpan");
        root.prepend(span);
        span.innerText = `Текущая цена: ${input.value}`;
        button = document.createElement("button");
        button.setAttribute("id", "priceButton");
        button.innerText = "(X)"
        span.after(button);
        document.getElementById("price").style.color = 'green';
        button.addEventListener("click", (event) => {
            document.getElementById("priceSpan").remove();
            document.getElementById("priceButton").remove();
        })
        document.getElementById("priceErr").remove();
    }
});

/*
Опишите своими словами, как Вы понимаете, что такое обработчик событий.

Обработчик событий - это функция, которая работает в том случае, если
указанное в ней событие произошло.

 */