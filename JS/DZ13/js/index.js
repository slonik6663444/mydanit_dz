"use strict"

const header = document.getElementById("main-menu");
const btn = document.createElement("button");

btn.getAttribute("type", "button");
btn.innerText = "Сменить тему";
header.appendChild(btn);
const chengeTheme=()=>{
    let head = document.head,
        link = document.createElement('link');
    link.rel = 'stylesheet';
    if (localStorage.getItem('theme') === 'darkTheme') {
        link.href = "css/dark.css";
    } else {
        link.href = "css/style.css";
    }
    head.appendChild(link)
}
btn.addEventListener("click", (evt) => {
    const theme=localStorage.getItem("theme")
    localStorage.setItem("theme",theme === "lightTheme" && "darkTheme" || "lightTheme");
    chengeTheme();
})
chengeTheme();
// 'use strict';
// // создал <link rel="stylesheet" href="light-theme|dark-theme.css">
// let head = document.head,
//     link = document.createElement('link');
// link.rel = 'stylesheet';
// // Проверка значения. Если dark-theme то будет темная тема
// if (localStorage.getItem('themeStyle') === 'dark') {
//     link.href = "css/dark.css"; // ссылка на темную тему
//     document.getElementById('switch-dark').setAttribute('checked', true); // переключаем чекбокс в положение "темная тема"
// }
//
// else {
//     link.href = '/assets/css/white-theme.css'; // ссылка на светлую тему
// }
// head.appendChild(link); // вставляем <link rel="stylesheet" href="light-theme|/assets/css/dark-theme.css">
//
// // событие при переключении чекбокса
// document.getElementById('switch-dark').addEventListener('change', ev => {
//     let btn = ev.target;
//     // если чекбокс включен
//     if (btn.checked) {
//         link.href = '/assets/css/dark-theme.css'; // включаем темную тему
//         localStorage.setItem('themeStyle', 'dark'); // записываем значение в localStorage
//     }
//     else {
//         link.href = '/assets/css/white-theme.css'; // включаем светлую тему
//         localStorage.setItem('themeStyle', 'light'); // записываем значение в localStorage
//     }
// });
















