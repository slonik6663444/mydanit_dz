"use strict"
const icon = [...document.getElementsByClassName("image-to-show")];
let count = 0;
let freeze = false;
const time = 10000;
const showIcon = () => {
    icon.forEach((element, index) => {
        const show = (index !== count);
        element.style = `display:${show ? "none" : "block"}`;
    })
    if (freeze) {
        freeze = false;
        return;
    }
    if (icon[count + 1]) {
        count += 1;
    } else {
        count = 0;
    }
}
showIcon();
let timer = setInterval(showIcon, time);
let btn = document.createElement("button");
const blockDiv = document.getElementsByTagName("body")[0];
btn.innerText = "Прекратить";
btn.addEventListener("click", (evt) => {
    freeze = true;
    clearInterval(timer);
})
blockDiv.appendChild(btn);

btn = document.createElement("button");
btn.innerText = "Возобновить показ";
btn.addEventListener("click", (evt) => {
    clearInterval(timer);
    timer = setInterval(showIcon, time);
})
blockDiv.appendChild(btn);


//1.Опишите своими словами разницу между функциями setTimeout() и setInterval().
// 2.Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
// 3.Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?

// 1. setTimeout() позволяет вызвать функцию один раз через заданное время. setInterval() позволяет вызывать функцию регулярно, повторяя вызов через  интервал времени, который задали.
// 2. Так как setTimeout() планирует выполнение функции, и нулевая задержка выполняться не будет.
// Таким образом, setTimeout() сработает только после завершения текущего кода.
// 3. Так как функция будет срабатывать каждый раз с заданным интервалом времени. А clearInterval() останавливает выполнение функции.










