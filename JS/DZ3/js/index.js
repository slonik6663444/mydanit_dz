"use strict"

let num1, num2, operation, result;

do {
    num1 = prompt("Enter your number 1");
    num2 = prompt("Enter your number 2");
} while (isNaN(+num1) ||
         num1 == "" ||
         isNaN(+num2) ||
         num2 == ""   )

operation = prompt("Enter operation +, -, *, /");

function counter() {

    switch (operation) {
        case "+" :
            result = (+num1 + +num2);
            console.log(result);
            break;
        case "-" :
            result = num1 - num2;
            console.log(result);
            break;
        case "*" :
            result = num1 * num2;
            console.log(result);
            break;
        case "/" :
            result = num1 / num2;
            console.log(result);
            break;

        default:
            break;
    }
}
counter(num1, num2, operation);

/*
1. Описать своими словами для чего вообще нужны функции в программировании.
Функции в програмировании используются для выполнения заданного порядка действий внутри самой функции. Порядок действий - это так называемые параметры функции,
которые остаются неизменными. А вот аргументы(значения переменных функции) можно задавать множество раз, как и вызывать саму функцию.
2. Описать своими словами, зачем в функцию передавать аргумент.
Аргумент необходимо передавать для вызова функции.
 */