import './App.css';
import React, {Component} from 'react';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {

    state = {
        showFirstModal: false,
        showSecondModal: false
    }

    openFirstModal = () => {
        this.setState({showFirstModal: !this.state.showFirstModal});
    }
    openSecondModal = () => {
        this.setState({showSecondModal: !this.state.showSecondModal});
    }
    closeModalWindow = (event) => {
        this.setState({showFirstModal: false});
        this.setState({showSecondModal: false});
    }

    render() {
        return (
            <>
                <div className='App'>
                    <Button text="Open first modal" backgroundColor='red' onClick={this.openFirstModal}></Button>
                    <Button text="Open second modal" backgroundColor='blue' onClick={this.openSecondModal}></Button>
                </div>

                {this.state.showFirstModal && <Modal onClick={(event) => this.closeModalWindow(event)}
                                                     header='Do you want to delete this file?'
                                                     text='Once you delete this file, it won’t be possible to undo this action Are you sure you want to delete it?'
                                                     actions={<>
                                                         <button className={'modal__window-btn'}>OK</button>
                                                         <button className={'modal__window-btn'}>Cancel</button>
                                                     </>} closeButton={true}>
                </Modal>}
                {this.state.showSecondModal && <Modal onClick={(event) => this.closeModalWindow(event)}
                                                      header='Do you want to save this file?' text='Bla-bla-bla?'
                                                      actions={<>
                                                          <button className={'modal__window-btn'}>Confirm</button>
                                                          <button className={'modal__window-btn'}>Cancel</button>
                                                      </>} closeButton={true}></Modal>}
            </>
        )
    }
}

export default App;