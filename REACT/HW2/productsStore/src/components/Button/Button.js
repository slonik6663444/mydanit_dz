import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './Button.scss';

class Button extends Component {
    render() {
        const { text, backgroundColor, onClick, className } = this.props;

        const styles = {
            backgroundColor
        }
        return (
            <button className={className} style={styles} onClick={onClick}>{text}</button>
        );
    }
}
Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}

Button.defaultProps = {
    backgroundColor: 'white'

}
export default Button;