import React, {Component} from 'react';
import './ProductCard.scss';
import PropTypes from 'prop-types';
import Button from "../Button/Button";
import Modal from "../Modal/Modal";

class ProductCard extends Component {

    state = {
        favorites: false,
        addToCart: false
    }

    funAddToCart = () => {
        this.setState({addToCart: !this.state.addToCart});
    }
    closeModalWindow = (event) => {
        this.setState({addToCart: false});
    }

    addToFavorite = (event) => {
        if (event.target.classList.contains('product-card__button')) {
            this.onClick(event);
            this.putToStorage('FavoritesProducts');
        } else {
            localStorage.setItem(`Favorites Product ${this.props.article}`, JSON.stringify(this.props))
            this.setState({
                favorites: true
            })
        }
    }

    removeToFavorite = () => {
        localStorage.removeItem(`Favorites Product ${this.props.article}`)
        this.setState({
            favorites: false
        })
    }

    putToStorage(folder) {
        let favoritesProducts = JSON.parse(localStorage.getItem(folder));
        if (favoritesProducts == null) favoritesProducts = [];
        favoritesProducts.push(this.props);
        let ProductsAddCart = Array.from(new Set(favoritesProducts.map(JSON.stringify))).map(JSON.parse);
        localStorage.setItem(folder, JSON.stringify(ProductsAddCart));
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (localStorage.getItem(`Favorites Product ${this.props.article}`)) {
            this.putToStorage('Favorites');
        }
    }


    render() {
        const {state} = this.state;
        const {name, price, pathToPic, article, color} = this.props;



        return (
            <>
                <div className='product-card__container'>
                    <img className='product-card__image' src={pathToPic} alt="{lipstick}"/>
                    <h1 className='product-card__title'>{name}</h1>
                    <h2 className='product-card__price'>{price}</h2>
                    <h2 className='product-card__article'>Article:{article}</h2>
                    <h2 className='product-card__color'>{color}</h2>
                    {state || localStorage.getItem(`Favorites Product ${this.props.article}`)
                        ?
                        <span className={"product-card__favorites-true"}
                              onClick={this.removeToFavorite}>
        <i className="far fa-star"/></span>
                        :
                        <span className={"product-card__favorites"}
                              onClick={(event) => this.addToFavorite(event)}>
        <i className="far fa-star"/></span>}


                    <Button  className='product-card__button' text='Add to cart' backgroundColor='pink' onClick={this.funAddToCart}></Button>
                </div>
                {this.state.addToCart && <Modal onClick={(event) => this.closeModalWindow(event)}
                                                header='Вы хотите добавить товар в корзину?' text='После подтверждения можете продолжить покупки'
                                                actions={<>
                                                    <button className={'modal__window-btn'} onClick={(event) => this.closeModalWindow(event)}>Продолжить покупки</button>
                                                    <button className={'modal__window-btn'}>Добавить в корзину</button>
                                                </>} closeButton={true}></Modal>}
            </>
        );
    }
}

export default ProductCard;

ProductCard.propTypes = {
    name: PropTypes.string,
    onClick: PropTypes.func,
    price: PropTypes.number,
    url: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
    favorite: PropTypes.bool
}

ProductCard.defaultProps = {
    price: 'none',
    article: 'none'
}
